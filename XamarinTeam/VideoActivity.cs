﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;

namespace XamarinTeam
{
    [Activity(Label = "VideoActivity")]
    public class VideoActivity : Activity
    {
        public VideoView MainVideoView { get; private set; }
        public VideoView VideoViewMain { get; private set; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_video);

            VideoViewMain = FindViewById<VideoView>(Resource.Id.videoViewMain);

            var videoPath = "http://distribution.bbb3d.renderfarming.net/video/mp4/bbb_sunflower_1080p_30fps_normal.mp4";

            var uri = Android.Net.Uri.Parse(videoPath);
            VideoViewMain.SetVideoURI(uri);

            var mediaController = new MediaController(this);
            VideoViewMain.SetMediaController(mediaController);
            mediaController.SetAnchorView(VideoViewMain);
        }
    }
}