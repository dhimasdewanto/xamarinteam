﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Text;
using Android.Util;
using System;
using static Android.App.DatePickerDialog;
using Android.Content;

namespace XamarinTeam
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity, IOnDateSetListener
    {
        public EditText EditTextName { get; private set; }
        public EditText EditTextDob { get; private set; }
        public Button ButtonInsert { get; private set; }
        public Button ButtonDeviceVideo { get; private set; }
        public Button ButtonStreamingVideo { get; private set; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            SetEditTextName();
            SetEditTextDob();
            SetButtonInsertClick();
            SetButtonVideoDeviceClick();
            SetButtonVideoStreamingClick();
        }

        private void SetButtonVideoDeviceClick()
        {
            ButtonDeviceVideo = FindViewById<Button>(Resource.Id.buttonDeviceVideo);
            ButtonDeviceVideo.Click += ButtonDeviceVideo_Click;
        }

        private void ButtonDeviceVideo_Click(object sender, EventArgs e)
        {
            var intent = new Intent(this, typeof(VideoActivity));
            StartActivity(intent);
        }

        private void SetButtonVideoStreamingClick()
        {
            ButtonStreamingVideo = FindViewById<Button>(Resource.Id.buttonStreamingVideo);
            ButtonStreamingVideo.Click += ButtonStreamingVideo_Click;
        }

        private void ButtonStreamingVideo_Click(object sender, EventArgs e)
        {
            var intent = new Intent(this, typeof(VideoStreamingActivity));
            StartActivity(intent);
        }

        private void SetButtonInsertClick()
        {
            ButtonInsert = FindViewById<Button>(Resource.Id.buttonInsert);
            ButtonInsert.Click += ButtonInsert_Click;
        }

        private void ButtonInsert_Click(object sender, System.EventArgs e)
        {
            Log.Info("Data", $"Name: {EditTextName.Text.ToString()}");
            Log.Info("Data", $"Date of Birth: {EditTextDob.Text.ToString()}");
            Log.Info("Data", $"Gender: {GetGender()}");
        }

        private void SetEditTextName()
        {
            EditTextName = FindViewById<EditText>(Resource.Id.editTextName);
            EditTextName.TextChanged += EditTextName_TextChanged;
        }

        private void EditTextName_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (EditTextName.Text.Length < 6)
            {
                EditTextName.Error = "Minimum Length is 6!";
            }
        }

        private void SetEditTextDob()
        {
            EditTextDob = FindViewById<EditText>(Resource.Id.editTextDob);
            EditTextDob.Click += EditTextDob_Click;
        }

        private void EditTextDob_Click(object sender, System.EventArgs e)
        {
            var dateTimeNow = DateTime.Now;
            DatePickerDialog datePicker = new DatePickerDialog(this, this, dateTimeNow.Year, dateTimeNow.Month, dateTimeNow.Day);
            datePicker.Show();
        }

        private string GetGender()
        {
            RadioButton RadioFemale = FindViewById<RadioButton>(Resource.Id.radioButtonFemale);
            RadioButton RadioMale = FindViewById<RadioButton>(Resource.Id.radioButtonMale);
            if (RadioFemale.Checked)
            {
                return "FEMALE";
            }
            if (RadioMale.Checked)
            {
                return "MALE";
            }
            return "";
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        public void OnDateSet(DatePicker view, int year, int month, int dayOfMonth)
        {
            EditTextDob.Text = new DateTime(year, month, dayOfMonth).ToShortDateString();
        }
    }
}