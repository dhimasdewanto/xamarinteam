﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Webkit;
using Android.Widget;

namespace XamarinTeam
{
    [Activity(Label = "VideoStreamingActivity")]
    public class VideoStreamingActivity : Activity
    {
        public WebView WebViewVideo { get; private set; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_videostreaming);

            WebViewVideo = FindViewById<WebView>(Resource.Id.webViewVideo);
            WebViewVideo.LoadUrl("http://distribution.bbb3d.renderfarming.net/video/mp4/bbb_sunflower_1080p_30fps_normal.mp4");
        }
    }
}